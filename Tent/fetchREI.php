<?php

$file = getcwd()."/reiURLs.txt";

$urls = file($file);

$xpathStrings = [
        'name'           => '//h1[@data-ui="product-information-title"]',
        'price'          => '//span[@itemprop="price"]',
        'packagedWeight' => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Packaged weight")]/td[1]',
        'minimumWeight'  => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Minimum trail weight")]/td[1]',
        'packedSize'     => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Packed size")]/td[1]',
        'designType'     => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Design type")]/td[1]',
        'vestibuleSize'  => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Vestibule area")]/td[1]',
        'floorArea'      => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Floor area")]/td[1]',
        'peakHeight'     => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Peak height")]/td[1]',
        'numberDoors'    => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Number of doors")]/td[1]',
        'numberPoles'    => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Number of poles")]/td[1]',
        'poleMaterial'   => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Pole material")]/td[1]',
        'canopy'         => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Canopy fabric")]/td[1]',
        'floor'          => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Floor fabric")]/td[1]',
        'rainfly'        => '//table[@data-ui="product-information-specifications"]//tr[contains(th[1], "Rainfly fabric")]/td[1]',
        'url'            => '',
    ];
    
    // setup columns for csv
    $csv[] = implode(',', array_keys($xpathStrings));
    unset($xpathStrings['url']); 
    //fwrite(STDOUT, "Column Headers:" . print_r($csv, true) . "\n");

foreach ($urls as $url) {
    fwrite(STDOUT, "Fetching URL: " . print_r($url, true));
    $page = file_get_contents($url);
    
    // Create DOM objects
    $dom = new DOMDocument();
    libxml_use_internal_errors(true);
    $dom->strictErrorChecking = false;
    $dom->recover = true;
    $dom->loadHTML($page);
    $domXpath = new DOMXPath($dom);
    
    // Grab table data from URL
    $values = [];
    foreach ($xpathStrings as $field => $xpath) {
        libxml_clear_errors();
        $domList = $domXpath->query($xpath);
         
        $error = libxml_get_last_error();
        if ($error !== false) {
            fwrite(STDERR, "libxml error: " . $error->message); 
        }

        if($domList->length === 0) {
            $values[] = 'no data';
        } else {
         
            //foreach ($domList as $node) {
            $values[] = trim($domList->item(0)->nodeValue);
            //}
        }
    }
    
    $values[] = $url;

    $csv[] = implode(',', $values);   
}

fwrite(STDOUT, "CSV array: \n" . print_r($csv, true) . "\n");

 $outputFile = fopen('./output.csv', 'w');
 
 foreach ($csv as $line) {
     fwrite($outputFile, $line."\n");
 }
 
 fclose($outputFile);
